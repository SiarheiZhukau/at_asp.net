﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webdriver.Tests
{
	[TestFixture]
	public class LabprojectTest
	{
		private IWebDriver driver;
		private readonly string domain = "http://labproject.somee.com";

		[SetUp]
		public void Initialize()
		{
			driver = new ChromeDriver();
			driver.Url = domain;
		}

		[Test]
		public void TitleTestTitleEqualsGoogle()
		{
			Assert.AreEqual(driver.Title, "Index");
		}

		[Test]
		public void ChooseLanguageTestResultBelarusian()
		{
			var langSelector = new SelectElement(driver.FindElement(By
				.Name("lang")));
			langSelector.SelectByValue("be");
			Assert.AreEqual(driver.Title, "Галоўная");
		}

		[Test]
		public void OnDateClickGoToPlayDetails()
		{
			var dateLink = driver.FindElement(By
				.XPath("//a[contains(@href, '/Home/Order/1')]"));
			
			dateLink.Click();
			Assert.AreEqual(driver.Title, "Order");
		}

		[TearDown]
		public void Cleanup()
		{
			driver.Quit();
		}
	}
}
