﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCalcLib
{
    public class Sequence : ISequence
    {
        public List<int> NextInts()
        {
            Random random = new Random();

            List<int> listOfInts = new List<int>();
            for (int i = 0; i < 10; i++)
            {
                listOfInts.Add(random.Next());
            }
            return listOfInts;
        }
    }
}
